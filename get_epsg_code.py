import sys
import os
import glob
from osgeo import gdal, osr, ogr


# https://gis.stackexchange.com/questions/7608/shapefile-prj-to-postgis-srid-lookup-table
def print_srs(file_path):
    try:
        lne_brk = '----------------------------------------------'

        # If input is file geodatabase.
        if os.path.basename(file_path).split('.')[-1] == 'gdb':

            # Change directory to parent dir of file geodatabase.
            output_dir = os.path.dirname(file_path)
            os.chdir(output_dir)

            # Redirect messages from console to log file.
            old_stdout = sys.stdout
            log_file_path = os.path.join(output_dir, "messages-epsg.log")
            print("Messages will be redirected from console to log file here: {}.".format(log_file_path))
            log_file = open(log_file_path, "w")
            sys.stdout = log_file

            # Open driver for reading file geodatabases.
            driver = ogr.GetDriverByName("OpenFileGDB")
            ds = driver.Open(file_path, 0)
            for fc_index in range(ds.GetLayerCount()):
                fc = ds.GetLayerByIndex(fc_index)
                srs = fc.GetSpatialRef()
                print(lne_brk)
                print('\nFeature Class: {}'.format(fc.GetName()), '\n')
                print('Proj4: {}'.format(srs.ExportToProj4()), '\n')
                print('WKT: {}'.format(srs.ExportToWkt()), '\n')
                srs.AutoIdentifyEPSG()
                print('EPSG code: {}'.format(srs.GetAuthorityCode(None)), '\n')

            # Close log file and return stout to default.
            sys.stdout = old_stdout
            log_file.close()

        # If input is a directory and is not a file geodatabase.
        if os.path.exists(file_path) and not os.path.basename(file_path).split('.')[-1] == 'gdb':

            # Change directory to file path.
            os.chdir(file_path)

            # Redirect messages from console to log file.
            old_stdout = sys.stdout
            log_file_path = os.path.join(file_path, "messages-epsg.log")
            print("Messages will be redirected from console to log file here: {}.".format(log_file_path))
            log_file = open(log_file_path, "w")
            sys.stdout = log_file

            # Get list of raster files in working directory.
            tif_list = glob.glob('*.tif')
            shp_list = glob.glob('*.shp')

            # If tif_list is not empty, loop through it.
            if len(tif_list) > 0:
                for tif in tif_list:
                    spatial_layer = gdal.Open(tif)
                    srs = spatial_layer.GetSpatialRef()
                    print(lne_brk)
                    print('\nLayer Name: {}'.format(tif), '\n')
                    print('Proj4: {}'.format(srs.ExportToProj4()), '\n')
                    print('WKT: {}'.format(srs.ExportToWkt()), '\n')
                    srs.AutoIdentifyEPSG()
                    print('EPSG code: {}'.format(srs.GetAuthorityCode(None)), '\n')

            # If shp_list is not empty, loop through it.
            if len(shp_list) > 0:
                driver = ogr.GetDriverByName('ESRI Shapefile')
                for shp in shp_list:
                    ds = driver.Open(shp, 0)
                    layer = ds.GetLayer()
                    srs = layer.GetSpatialRef()
                    print(lne_brk)
                    print('\nLayer Name: {}'.format(shp))
                    print('Proj4: {}'.format(srs.ExportToProj4()), '\n')
                    print('WKT: {}'.format(srs.ExportToWkt()), '\n')
                    srs.AutoIdentifyEPSG()
                    print('EPSG code: {}'.format(srs.GetAuthorityCode(None)), '\n')

            # Close log file and return stout to default.
            sys.stdout = old_stdout
            log_file.close()

    except Exception as e:
        print(e.message, e.args)


print_srs(sys.argv[1])
