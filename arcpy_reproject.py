# Import modules.
import arcpy
import os
import sys


def create_outdir(out_dir_name):
    """Create output directory up one level from where script is executed.

    :param out_dir_name: name for output directory to create one level above where script is run from.
    :return: [0] root_dir: path to directory one level above script file.
             [1] out_dir: path to masked output directory.
    """
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    os.chdir("..")
    root_dir = os.getcwd()

    # Check if output directory already exists, if not, create it.
    if not os.path.exists(out_dir_name):
        out_dir = os.path.join(root_dir, out_dir_name)
        print("Creating output directory here: {}".format(out_dir))
        os.mkdir(out_dir_name)
    else:
        # Exit program if folder exists.
        print("{} folder already exists. Exiting program.".format(out_dir_name))
        sys.exit()

    # Return paths.
    return root_dir, out_dir


def reproject(input_gdb, output_dir, epsg_code):
    """https://desktop.arcgis.com/en/arcmap/10.3/tools/data-management-toolbox/project.htm#C_GUID-7F18568B-C23E-4603-85AC-E5BD68F81369

    :param input_gdb: path to input gdb.
    :param output_dir: output dir path.
    :param epsg_code: EPSG code.
    :return:
    """
    # Make new gdb. Get name from input geodatabase.
    gdb_name = os.path.basename(input_gdb)
    print("Creating output geodatabase here: {}".format(os.path.join(output_dir, gdb_name)))
    arcpy.CreateFileGDB_management(output_dir, gdb_name)

    ln_brk = "\n--------------------------------------------------\n"

    # Set workspace.
    arcpy.env.workspace = input_gdb

    # Set local variables.
    output_workspace = os.path.join(output_dir, gdb_name)

    try:
        # Use ListFeatureClasses to generate a list of inputs .
        for in_fc in arcpy.ListFeatureClasses():

            # Determine if the input has a defined coordinate system, can't project it if it does not.
            dsc = arcpy.Describe(in_fc)

            if dsc.spatialReference.Name == "Unknown":
                print('skipped this fc due to undefined coordinate system: ' + in_fc)
            else:
                # Determine the new output feature class path and name.
                out_fc = os.path.join(output_workspace, in_fc)

                # Set output coordinate system.
                out_sr = arcpy.SpatialReference(int(epsg_code))

                # Run project tool.
                print(ln_brk)
                print("Projecting {} with spatial reference system: {}.\n".format(out_fc, str(epsg_code)))
                arcpy.Project_management(in_fc, out_fc, out_sr)

                # Print messages.
                print(arcpy.GetMessages())

    except arcpy.ExecuteError:
        print(arcpy.GetMessages(2))

    except Exception as ex:
        print(ex.args[0])


def main():
    output_directory = create_outdir("reprojected_data")
    input_geodatabase = raw_input("Full path to input geodatabase: ")
    epsg_code = raw_input("EPSG code such as 4326: ")
    reproject(input_geodatabase, output_directory[1], epsg_code)


if __name__ == "__main__":
    main()
