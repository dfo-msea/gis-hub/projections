# Import modules.
# download https://github.com/Esri/file-geodatabase-api
# setx GDAL_DRIVER_PATH "C:\Users\FieldsC\AppData\Local\Continuum\anaconda3\envs\gis-env\Lib\site-packages\osgeo"

from osgeo import ogr, osr, gdal
import sys
import os
import glob
import time
import getpass
import socket
from datetime import date


def print_drivers(): # https://pcjericks.github.io/py-gdalogr-cookbook/layers.html
    """
    Print alphabetized list of drivers from OGR
    :return:
    """
    cnt = ogr.GetDriverCount()
    formats_list = []  # Empty List

    for i in range(cnt):
        driver = ogr.GetDriver(i)
        driver_name = driver.GetName()
        if not driver_name in formats_list:
            formats_list.append(driver_name)

    formats_list.sort() # Sorting the messy list of ogr drivers

    for i in formats_list:
        print(i)


def open_filegeo(path_to_fgdb, output_dir):
    """

    :param path_to_fgdb:
    :param output_dir:
    :return:
    """
    try:
        # Start output log file.
        old_stdout = sys.stdout
        log_name = "log_" + os.path.basename(path_to_fgdb).split('.')[0] + ".log"
        log_file_path = os.path.join(output_dir, log_name)
        print("Messages will be redirected from console to log file here: {}.".format(log_file_path))
        log_file = open(log_file_path, "w")
        sys.stdout = log_file
        line_format = "-------------------------"
        # Iterate feature classes and print Spatial Reference Information.
        driver = ogr.GetDriverByName("OpenFileGDB")
        ds = driver.Open(path_to_fgdb, 0)
        for fc_index in range(ds.GetLayerCount()):
            fc = ds.GetLayerByIndex(fc_index)
            srs = fc.GetSpatialRef()
            print(line_format, fc.GetName(), line_format)
            print(srs)
            print("PROJ4: {}".format(srs.ExportToProj4()))
        # Close log file and return stout to default.
        sys.stdout = old_stdout
        log_file.close()
    except Exception as e:
        print(e.args)
    finally:
        # Close log file and return stout to default.
        sys.stdout = old_stdout
        log_file.close()


def reproject_tiff(working_dir, output_dir, out_srs):
    """ Reproject GeoTIFF rasters to new projection.

    :param working_dir: working directory with GeoTIFF files.
    :param output_dir: outpur directory for new rasters.
    :param out_srs: EPSG code in the form 'EPSG: <code>' such as 'EPSG: 3005'
    :return:
    """
    try:
        # Start timer for interpolation.
        start = time.time()

        # Check if output directory already exists, if not, create it.
        if not os.path.exists(output_dir):
            print("Creating output directory here: {}".format(output_dir))
            os.mkdir(output_dir)
        else:
            print("{} folder already exists. Output will be directed here.".format(output_dir))

        # Redirect messages from console to log file.
        old_stdout = sys.stdout
        log_file_path = os.path.join(output_dir, "messages-reproject.log")
        print("Messages will be redirected from console to log file here: {}.".format(log_file_path))
        log_file = open(log_file_path, "w")
        sys.stdout = log_file

        # Print break line.
        brk_line = "------------------------------------------------------------------\n"
        # Print details about script run to user.
        print(brk_line)
        print("Script executed on: {} by {} on {}.\n".format(date.today(), getpass.getuser(), socket.gethostname()))
        print(brk_line)
        # Set working directory.
        print("Working Directory: {}".format(working_dir))
        print("Output Directory: {}".format(output_dir))
        print("Spatial Reference System: {}\n".format(out_srs))
        os.chdir(working_dir)
        # Get list of raster files in working directory.
        raster_list = glob.glob('*.tif')
        # Counter variable.
        counter = 1
        # Loop through raster files and reproject to desired spatial reference system.
        for ras in raster_list:
            print(brk_line)
            print("Working on file {} of {}...".format(counter, len(raster_list)))
            in_ras = gdal.Open(ras)
            # Get basename of raster file.
            out_ras_name = os.path.basename(ras)
            out_ras = os.path.join(output_dir, out_ras_name)
            print("Reprojecting {} to {} spatial reference system...".format(ras, out_srs))
            gdal.Warp(out_ras, in_ras, dstSRS=out_srs, copyMetadata=True, format="GTiff")
            # Read raster in read-only mode (0) to calculate and save ovr file as external file.
            out_ras = gdal.Open(out_ras, 0)
            print("Calculating statistics and saving to aux.xml file...")
            gdal.Info(out_ras, stats=True)
            print("Building overviews and saving as .ovr file...")
            out_ras.BuildOverviews("NEAREST", [2, 4, 8, 16, 32, 64])
            print("Processing file {} complete.\n".format(counter))
            # Increment counter variable.
            counter += 1

        end = time.time()
        hours, rem = divmod(end - start, 3600)
        minutes, seconds = divmod(rem, 60)
        print(brk_line)
        print("Processing time:\n{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))

        # Close log file and return stout to default.
        sys.stdout = old_stdout
        log_file.close()

    except Exception as e:
        print(e.message, e.args)


def main():
    working_directory = input("Path to input directory: ")
    output_directory = input("Path to output directory: ")
    epsg_code = input("EPSG code in the form 'EPSG:<code>' such as 'EPSG:3005': ")
    reproject_tiff(working_directory, output_directory, epsg_code)


if __name__ == "__main__":
    main()
