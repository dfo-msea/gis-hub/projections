import os
import datetime
import logging
import arcpy


def get_rasters(dir_prompt, logger):
    """Get path to file geodatabase.
    :param dir_prompt: Filepath to file geodatabase.
    :return: list of rasters
    """
    fgdb_path = os.path.normpath(input(dir_prompt))


    # Check if output directory already exists, if not, create it.
    if not os.path.isdir(fgdb_path):
        logger.error(f"{fgdb_path} does not exist.")
        exit()
    else:
        logger.info(f"Setting environment to: {fgdb_path}")
        arcpy.env.workspace = fgdb_path
        logger.info(f"Getting list of rasters from: {fgdb_path}")
        rasters = arcpy.ListRasters()
        logger.info("Raster layers:")
        [logger.info(f"{index:3}. {ras}") for index, ras in enumerate(rasters, start=1)]
        return rasters


def copy_rasters(raster_list, logger, output_directory):
    """MGet path to file geodatabase.
    :param raster_list: list of rasters in file geodatabase.
    :return: None
    """
    try:
        logger.info(f"Exporting rasters to {output_directory} in GeoTIFF format and assiging NoData value (-9999)...")
        num_files = len(raster_list)
        for index, ras in enumerate(raster_list, start=1):
            logger.info(f" - Processing file {index:2} of {num_files}: {ras}")
            # Copy raster to output directory with same name, in geotiff format, assign nodata value
            ras_name = os.path.join(output_directory, ras + ".tif")
            arcpy.CopyRaster_management(in_raster=ras,
                                        out_rasterdataset=ras_name,
                                        nodata_value=-9999)
        logger.info("Exporting rasters: Complete.")
        return
    except:
        logger.error("Export raster failed.")
        logger.error(arcpy.GetMessages())


def logger_setup(output_directory):
    logging.basicConfig(
        filename=os.path.join(output_directory, f"log-{datetime.datetime.now():%Y-%m-%d}.txt"),
        format="|%(levelname)-9s|%(asctime)s| %(module)s(%(lineno)d)| %(message)s",
        level="INFO",  # numeric value: 20
    )
    logger = logging.getLogger('gdal_translate')
    return logger


def mk_dir(dir_prompt):
    """Make an output directory.
    :param dir_prompt: Filepath to existing directory or filepath with directory to create.
    :return: dir_path
    """
    dir_path = os.path.normpath(input(dir_prompt))

    # Check if output directory already exists, if not, create it.
    if not os.path.exists(dir_path):
        print(f"Creating output directory here: {dir_path}")
        os.makedirs(dir_path)
        return dir_path
    else:
        print(f"Output directory ({dir_path}) already exists.")
        return dir_path


def main():
    out_dir = mk_dir("Enter an output directory path: ")
    logger = logger_setup(out_dir)
    raster_files = get_rasters("Enter a file geodatabase path: ", logger)
    geotiff_files = copy_rasters(raster_files, logger, out_dir)


if __name__ == '__main__':
    main()
