# projections

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: cole.fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The purpose of these scripts is to determine if spatial layers are missing EPSG codes in their spatial reference system, and reproject data using EPSG codes as the authority. This step of checking for EPSG codes should be included in the QA/QC process of layers published to the GIS Hub. get_epsg_code will save a log file that contains the spatial reference system information for input datasets. arcpy_reproject works with file geodatabase feature classes. project_tiffs works on a directory of GeoTIFF files.


## Summary
Datasets may have incomplete headers in the spatial reference system information associated with the spatial data. For example, some datasets may have the same parameters as BC Albers, but are missing the authority code information (EPSG: 3005). Opening these layers in ArcGIS will show that they have a custom authority. These scripts are designed to 1) determine if spatial reference system is missing the EPSG code 2) reproject data using an EPSG code.

GeoServer - one component of the GIS-Hub - cannot handle these layers without an EPSG code. Therefore, no web map preview can be generated for these layers. It may be determined that a dataset intentionally has a custom projections and therefore no EPSG code is associated with it. This is fine, there will just be no web map preview generated for the dataset.


## Status
Completed


## Contents
There are 3 independent scripts in this repository that are designed to be executed using the command line interface. They will prompt the user for input information as required. There is minimal error checking handling for the arguments provided by the user and therefore unknown errors may occur. 

### get_epsg_code
Outputs a messages.log file in the parent directory of input dataset (if dataset is a file geodatabase) or in the input directory file path provided by the user as input. The log file will contain the WKT, Proj4, and EPSG code of the spatial reference system for each layer in the dataset or directory.

### reproject_tiffs 
Projects GeoTIFF files in a working directory to a new output directory using EPSG code. Outputs a log file for documentation. Recommend using Python 3.x. Does not require ArcPy module.

### arcpy_reproject 
Projects feature classes in a file geodatabase (fgdb) to a new fgdb. New fgdb is generated inside a new directory (called "reprojected_data") created one level above where the script is located. Requires ArcPy module to write to a fgdb. 

## Methods

### get_epsg_code
* Change dir to parent directory of fgdb if input is fgdb. 
* If input is a directory, change to that dir.
* Start a log file.
* Iterate through layers (feature classes in fgdb, shapefiles in dir, or Geotiffs in dir).
* Print layer name, WKT, Proj4, and EPSG code for each layer to log file.
* Save log file to disk. 
---
Run get_epsg_code
1. open anaconda prompt
2. create a new environment by typing `conda create --name proj python=3 -y`. The -y tells it to download and install packages to the new env.
3. to list all the envs you have on your machine: `conda env list`
4. to activate an env: `conda activate proj`. This will activate your new environment.
5. install gdal to proj env: `conda install gdal -y`
6. clone repo to machine
7. with your environment activated, type `python` followed by the path to get_epsg_code.py followed by a path to a fgdb with feature classes, a directory with shapefiles or a directory with Geotiff files. Example: `python D:\Projects\gis-hub\Scripts\projections\code\get_epsg_code.py C:\Temp\roms`
8. The messages.log file will be saved in the given directory or in the parent dir if the input is a fgdb.



### reproject_tiffs 
* Changes directory to working directory (where GeoTIFF files are located on disk). 
* Checks for the existence of specified output directory. If it already exists, output will be directed here. If directory does not exist, it will be created first and then output directed there. 
* Starts a log file.
* Loops through list of raster files and reprojects each one using gdal.Warp to a new spatial reference system using the specified EPSG code.
* Also calculated statistics and generates overviews for each layer.

### arcpy_reproject 
* Create new directory called reprojected_data one level above where script is located on disk. 
* Generates a new fgdb (with same name as original input fgdb) in new output directory.
* Loops through input fgdb feature classes and reprojects them to specified EPSG code.


## Requirements

### get_epsg_code (Python 3.x)
- import sys
- import os
- import glob
- from
- osgeo import gdal, osr, ogr

### reproject_tiffs (Python 3.x)
- from osgeo import ogr, osr, gdal
- import sys
- import os
- import glob
- import time
- import getpass
- import socket
- from datetime import date


### arcpy_reproject (Python 2.x with ArcPy module)
- import arcpy
- import os
- import sys



## Caveats
* Only tested with GeoTIFF files and feature classes inside a file geodatabase. Not tested with shapefiles, geopackages, or raster formats other than GeoTIFF.
* If any raster attribute tables exist in original raster files, they are not regenerated in the output rasters. 


## Uncertainty
* Pyramids (overviews) generated using NEAREST method. Possibly not suitable for categorical rasters. 


## Acknowledgements
https://pcjericks.github.io/py-gdalogr-cookbook/layers.html
https://gdal.org/programs/gdaladdo.html

## References
NA
